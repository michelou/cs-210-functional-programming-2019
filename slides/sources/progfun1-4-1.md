% Decomposition
%
%

Decomposition
=============

Suppose you want to write a small interpreter for arithmetic
expressions.

To keep it simple, let's restrict ourselves to numbers and
additions.

Expressions can be represented as a class hierarchy, with a base trait
`Expr` and two subclasses, `Number` and `Sum`.

To treat an expression, it's necessary to know the expression's shape
and its components.

This brings us to the following implementation.

Expressions
===========

      trait Expr {
        def isNumber: Boolean
        def isSum: Boolean
        def numValue: Int
        def leftOp: Expr
        def rightOp: Expr
      }
      class Number(n: Int) extends Expr {
        def isNumber: Boolean = true
        def isSum: Boolean = false
        def numValue: Int = n
        def leftOp: Expr = throw Error("Number.leftOp")
        def rightOp: Expr = throw Error("Number.rightOp")
      }

Expressions (2)
===============

      class Sum(e1: Expr, e2: Expr) extends Expr {
        def isNumber: Boolean = false
        def isSum: Boolean = true
        def numValue: Int = throw Error("Sum.numValue")
        def leftOp: Expr = e1
        def rightOp: Expr = e2
      }

Evaluation of Expressions
=========================

You can now write an evaluation function as follows.

      def eval(e: Expr): Int =
        if e.isNumber then e.numValue
        else if e.isSum then eval(e.leftOp) + eval(e.rightOp)
        else throw Error("Unknown expression " + e)

\red{Problem}: Writing all these classification and accessor functions
quickly becomes tedious!

Adding New Forms of Expressions
===============================

So, what happens if you want to add new expression forms, say

      class Prod(e1: Expr, e2: Expr) extends Expr   // e1 * e2
      class Var(x: String) extends Expr             // Variable `x'

You need to add methods for classification and access to all classes
defined above.

Question
========

To integrate `Prod` and `Var` into the hierarchy, how many new
method definitions do you need?

(including method definitions in `Prod` and `Var` themselves, but not counting methods that were already given on the slides)

Possible Answers

     O            9
     O           10
     O           19
     O           25
     O           35
     O           40

->
\quiz

Non-Solution: Type Tests and Type Casts
=======================================

A "hacky" solution could use type tests and type casts.

Scala let's you do these using methods defined in class `Any`:

       def isInstanceOf[T]: Boolean  // checks whether this object's type conforms to `T`
       def asInstanceOf[T]: T        // treats this object as an instance of type `T`
                                     // throws `ClassCastException` if it isn't.

These correspond to Java's type tests and casts

       Scala                   Java

       x.isInstanceOf[T]       x instanceof T
       x.asInstanceOf[T]       (T) x

But their use in Scala is discouraged, because there are better alternatives.

Eval with Type Tests and Type Casts
===================================

Here's a formulation of the `eval` method using type tests and casts:

        def eval(e: Expr): Int =
          if e.isInstanceOf[Number] then
            e.asInstanceOf[Number].numValue
          else if e.isInstanceOf[Sum] then
            eval(e.asInstanceOf[Sum].leftOp)
            + eval(e.asInstanceOf[Sum].rightOp)
          else throw Error("Unknown expression " + e)

Assessment of this solution:
->
\begin{tabular}{lp{10cm}}
 + & no need for classification methods, access methods only for classes where the value is defined.
\\
 -- & low-level and potentially unsafe.
\end{tabular}

Solution 1: Object-Oriented Decomposition
=========================================

For example, suppose that all you want to do is _evaluate_ expressions.

You could then define:

      trait Expr {
        def eval: Int
      }
      class Number(n: Int) extends Expr {
        def eval: Int = n
      }
      class Sum(e1: Expr, e2: Expr) extends Expr {
        def eval: Int = e1.eval + e2.eval
      }

But what happens if you'd like to display expressions now?

You have to define new methods in all the subclasses.

Assessment of OO Decomposition
==============================

 - OO decomposition mixes _data_ with _operations_ on the data.
 - This can sometimes be good for encapsulation.
 - On the other hand, it increases complexity(*) and adds new dependencies to classes.
 - It makes it easy to add new kinds of data but hard to add new kinds of operations.

(*) In the original sense of the word:

> _complex = plaited_ (_tressé_ in French).

Thus, complexity arises from mixing several things together.


Limitations of OO Decomposition
===============================

OO decomposition only works well if operations are on a single object.

What if you want to simplify expressions, say using the rule:

      a * b + a * c   ->   a * (b + c)

\red{Problem}: This is a non-local simplification. It cannot be encapsulated in the
method of a single object.

You are back to square one; you need test and access methods for all the different
subclasses.


