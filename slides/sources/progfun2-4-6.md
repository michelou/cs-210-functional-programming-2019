% Implicit Conversions
%
%

Implicit Conversions
====================

**Implicit conversions** make it possible to convert an expression to a different
type.

This mechanism is usually used to provide more ergonomic APIs.

->

Example: API for defining JSON documents.

~~~
// { "name": "Paul", "age": 42 }
Json.obj("name" -> "Paul", "age" -> 42)
~~~

Type Coercion: Motivation (1)
=============================

~~~
sealed trait Json
case class JNumber(value: BigDecimal) extends Json
case class JString(value: String) extends Json
case class JBoolean(value: Boolean) extends Json
case class JArray(elems: List[Json]) extends Json
case class JObject(fields: (String, Json)*) extends Json
~~~

->

~~~
// { "name": "Paul", "age": 42 }
JObject("name" -> JString("Paul"), "age" -> JNumber(42))
~~~

Problem: Constructing JSON objects is too verbose.

Type Coercion: Motivation (2)
=============================

~~~
sealed trait Json
case class JNumber(value: BigDecimal) extends Json
case class JString(value: String) extends Json
case class JBoolean(value: Boolean) extends Json
case class JArray(elems: List[Json]) extends Json
case class JObject(fields: (String, Json)*) extends Json
~~~

~~~
// { "name": "Paul", "age": 42 }
Json.obj("name" -> "Paul", "age" -> 42)
~~~

How could we support the above user-facing syntax?

Type Coercion: Motivation (3)
=============================

~~~
// { "name": "Paul", "age": 42 }
Json.obj("name" -> "Paul", "age" -> 42)
~~~

What could be the type signature of the `obj` constructor?

->

~~~
def obj(fields: (String, Any)*): Json
~~~

->

Allows invalid JSON objects to be constructed!

~~~
Json.obj("name" -> ((x: Int) => x + 1))
~~~

We want invalid code to be signaled to the programmer with a
compilation error.

Type Coercion (1)
=================

~~~
object Json {

  def obj(fields: (String, JsonField)*): Json =
    JObject(fields.map(_.toJson): _*)
    
  trait JsonField {
    def toJson: Json
  }

}
~~~

Type Coercion (2)
=================

~~~  
trait JsonField {
  def toJson(): Json
}

object JsonField {
  given stringToJsonField: Conversion[String, JsonField] =
    (s: String) => () => JString(s)
  given intToJsonField: Conversion[Int, JsonField] =
    (n: Int) => () => JNumber(n)
  ...
  given jsonToJsonField: Conversion[JSon, JsonField] =
    (j: Json) => () => j
}
~~~

Type Coercion: Usage
====================

~~~
Json.obj("name" -> "Paul", "age" -> 42)
~~~

->

The compiler implicitly inserts the following conversions:

~~~
Json.obj(
  "name" -> Json.JsonField.stringToJsonField.apply("Paul"),
  "age" -> Json.JsonField.intToJsonField.apply(42)
)
~~~

Implicit Conversions
====================

The compiler looks for implicit conversions on an expression `e` of type `T`
if `T` does not conform to the expression’s expected type `S`.

In such a case, the compiler looks in the implicit scope for a given instance of
type `Conversion[T, S]`.

Note: at most one implicit conversion can be applied to a given expression.

Summary
=======

- Implicit conversions can improve the ergonomics of an API
